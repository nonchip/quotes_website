RegexRouter = require 'dysnomia.routers.regex_router'
HDSL= require 'dysnomia.renderers.html5_dsl'
config = require 'dysnomia.config'

import pull from require 'backend'

with RegexRouter!
  \add '/push', ->
    pull!
    ngx.exit(ngx.OK)

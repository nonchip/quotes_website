import dump from require'moonscript.util'

escape_pattern = do
  punct = "[%^$()%.%[%]*+%-?]"
  (str) ->
    (str\gsub punct, (p) -> "%"..p)
html_escape_entities = {
  ['&']: '&amp;'
  ['<']: '&lt;'
  ['>']: '&gt;'
  ['"']: '&quot;'
  ["'"]: '&#039;'
}
html_escape_pattern = "[" .. table.concat([escape_pattern char for char in pairs html_escape_entities]) .. "]"
escape = (text) ->
  (text\gsub html_escape_pattern, html_escape_entities)


require'dysnomia.renderers.html5_dsl' =>
  if @results and #@results >0
    ul ->
      for result in *@results
        li ->
          pre escape result -- todo: figure out why dysnomia doesn't escape this 0.o
  else
    em ->
      RAW 'your search'
      if @query
        RAW ' for '
        tt @query
      RAW ' returned no results'
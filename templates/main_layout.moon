require'dysnomia.renderers.html5_dsl' =>
  HTML5 ->
    style[[
      body, *{
        box-sizing: border-box;
      }
      ul{
        list-style-type: none;
        margin: 0;
        padding: 0;
      }
      li{
        border: 1px solid #555;
        background: #eee;
        margin: 5px;
        padding: 5px 15px;
        float: left;
        clear: left;
      }
    ]]
    head ->
      title (@title and @title..' - ' or '')..'quotes.nonchip.de'
    body ->
      RAW @content or 'here be dragons'

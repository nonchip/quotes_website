RegexRouter = require 'dysnomia.routers.regex_router'
HDSL= require 'dysnomia.renderers.html5_dsl'
config = require 'dysnomia.config'

import search from require 'backend'

main_layout = require 'templates.main_layout'
search_form_tpl = require 'templates.search_form'
search_results_tpl = require 'templates.search_results'

mkaction_search= (form)->
  (matches)->
    query = matches and matches[1]
    ngx.req.read_body!
    args,err = ngx.req.get_post_args!
    query = args.query if args and args.query and not query

    content = form and (search_form_tpl\render{:query}) or ''

    if query
      content ..= search_results_tpl\render {:query, results: search query}

    ngx.say main_layout\render {
      title: query and 'Search: '..query or 'Search'
      :content
    }
    ngx.exit(ngx.OK)


with RegexRouter!
  \add '', mkaction_search true
  \add 'search', mkaction_search true
  \add '(.*)', mkaction_search false

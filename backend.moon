lfs=require'lfs'

config = require 'dysnomia.config'

prefix = './'

csplit = (str,sep)->
   ret={}
   n=1
   for w in string.gmatch str,"([^#{sep}]*)"
      ret[n] = ret[n] or w
      if w=="" then
         n = n + 1
   ret


pull = ->
  os.execute "cd #{prefix}backend_repo; git pull"

search= (query)->
  results={}
  path = "#{prefix}backend_repo/quotes"
  subqueries=csplit query, ';'
  for file in lfs.dir path
    fp,err=io.open path..'/'..file, 'r'
    if fp
      fstr = fp\read '*all'
      if fstr
        for subquery in *subqueries
          if string.match fstr, subquery
            table.insert results, fstr
      fp\close!
  results

{:search, :pull}

->
  app ->
    debug true
  ngx ->
    debug true
    daemon false
    worker_processes 1
    events ->
      worker_connections 10
    http ->
      server ->
        port 8080
        lua_code_cache false
